
import java.util.Scanner;
import java.util.InputMismatchException;
public class Sonde  extends Composant{
	protected Composant comp;
	protected String str;
	Scanner sc = new Scanner(System.in);

  public String description(){
  return(getId());
  }  
	Sonde(Composant comp, String str){
	 this.str = str;
	 this.comp = comp;
	}

	public boolean getEtat() throws InputMismatchException {
	System.out.println(str.toString()+" de " +comp.getId() +" true or false?");
	try{
        boolean b = sc.nextBoolean();
         return b;
	   }catch(InputMismatchException ex){
	   	System.out.println("Erreur d'initialisation");
         return false;
	   }
	}
} 
