

import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;
public class SondesTable{

  HashMap<LazySonde,Interrupteur> sondesToInterrupteurs = new HashMap<LazySonde,Interrupteur>();
  HashMap<Interrupteur,LazySonde> interrupteursToSonde = new HashMap<Interrupteur,LazySonde>();

public Interrupteur getInterrupteur(LazySonde sonde){

    Set set = sondesToInterrupteurs.entrySet();
    Iterator iterator = set.iterator();

    while(iterator.hasNext()) {
         Map.Entry mentry = (Map.Entry)iterator.next();
         if(mentry.getKey() == sonde){
         	return (Interrupteur)mentry.getValue();
         }
      }

 return null;

}  

public LazySonde getSonde(Interrupteur interrupteur, Composant cible, String entree){

	Set set = interrupteursToSonde.entrySet();
    Iterator iterator = set.iterator();

    while(iterator.hasNext()) {
         Map.Entry mentry = (Map.Entry)iterator.next();
         if((mentry.getKey() != null) && (mentry.getKey() == interrupteur)){
         	return (LazySonde)mentry.getValue();
         	
         }
      }
      LazySonde sonde = new LazySonde(cible,entree);
      interrupteursToSonde.put(interrupteur,sonde);
      sondesToInterrupteurs.put(sonde,interrupteur);
      return sonde;
}

public void resetSondes(){
	Set set = interrupteursToSonde.entrySet();
    Iterator iterator = set.iterator();

    while(iterator.hasNext()) {
         Map.Entry mentry = (Map.Entry)iterator.next();
         	((LazySonde)mentry.getValue()).reset();	
         }
 }

public void clear(){
	interrupteursToSonde.clear();
    sondesToInterrupteurs.clear();
}
	
} 
