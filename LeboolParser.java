// Generated from LeboolParser.g4 by ANTLR 4.5
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class LeboolParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		RETURNS=1, END=2, DESCR=3, TRUEBOOL=4, FALSEBOOL=5, EVAL=6, LPAR=7, RPAR=8, 
		AND=9, OR=10, NOT=11, EQUAL=12, VIRG=13, PVIRG=14, ID=15, WS=16;
	public static final int
		RULE_prog = 0, RULE_commandes = 1, RULE_liste_id = 2, RULE_booleen = 3, 
		RULE_liste_booleen = 4, RULE_expr = 5, RULE_eq = 6, RULE_equations = 7;
	public static final String[] ruleNames = {
		"prog", "commandes", "liste_id", "booleen", "liste_booleen", "expr", "eq", 
		"equations"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'returns'", "'end'", "'descr'", "'true'", "'false'", "'eval'", 
		"'('", "')'", "'&'", "'|'", "'!'", "'='", "','", "';'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "RETURNS", "END", "DESCR", "TRUEBOOL", "FALSEBOOL", "EVAL", "LPAR", 
		"RPAR", "AND", "OR", "NOT", "EQUAL", "VIRG", "PVIRG", "ID", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "LeboolParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	 EquationCircuit monCircuit = new EquationCircuit();
	public LeboolParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgContext extends ParserRuleContext {
		public Liste_idContext liste_id;
		public TerminalNode ID() { return getToken(LeboolParser.ID, 0); }
		public List<TerminalNode> LPAR() { return getTokens(LeboolParser.LPAR); }
		public TerminalNode LPAR(int i) {
			return getToken(LeboolParser.LPAR, i);
		}
		public List<Liste_idContext> liste_id() {
			return getRuleContexts(Liste_idContext.class);
		}
		public Liste_idContext liste_id(int i) {
			return getRuleContext(Liste_idContext.class,i);
		}
		public List<TerminalNode> RPAR() { return getTokens(LeboolParser.RPAR); }
		public TerminalNode RPAR(int i) {
			return getToken(LeboolParser.RPAR, i);
		}
		public TerminalNode RETURNS() { return getToken(LeboolParser.RETURNS, 0); }
		public EquationsContext equations() {
			return getRuleContext(EquationsContext.class,0);
		}
		public TerminalNode END() { return getToken(LeboolParser.END, 0); }
		public CommandesContext commandes() {
			return getRuleContext(CommandesContext.class,0);
		}
		public ProgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prog; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LeboolParserListener ) ((LeboolParserListener)listener).enterProg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LeboolParserListener ) ((LeboolParserListener)listener).exitProg(this);
		}
	}

	public final ProgContext prog() throws RecognitionException {
		ProgContext _localctx = new ProgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prog);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(16);
			match(ID);
			setState(17);
			match(LPAR);
			setState(18);
			((ProgContext)_localctx).liste_id = liste_id(0);
			monCircuit.setInputs(((ProgContext)_localctx).liste_id.names);
			setState(20);
			match(RPAR);
			setState(21);
			match(RETURNS);
			setState(22);
			match(LPAR);
			setState(23);
			((ProgContext)_localctx).liste_id = liste_id(0);
			monCircuit.setOutputs(((ProgContext)_localctx).liste_id.names);
			setState(25);
			match(RPAR);
			setState(26);
			equations();
			setState(27);
			match(END);
			setState(28);
			commandes();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommandesContext extends ParserRuleContext {
		public Liste_booleenContext liste_booleen;
		public TerminalNode DESCR() { return getToken(LeboolParser.DESCR, 0); }
		public TerminalNode LPAR() { return getToken(LeboolParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(LeboolParser.RPAR, 0); }
		public TerminalNode EVAL() { return getToken(LeboolParser.EVAL, 0); }
		public Liste_booleenContext liste_booleen() {
			return getRuleContext(Liste_booleenContext.class,0);
		}
		public CommandesContext commandes() {
			return getRuleContext(CommandesContext.class,0);
		}
		public CommandesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_commandes; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LeboolParserListener ) ((LeboolParserListener)listener).enterCommandes(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LeboolParserListener ) ((LeboolParserListener)listener).exitCommandes(this);
		}
	}

	public final CommandesContext commandes() throws RecognitionException {
		CommandesContext _localctx = new CommandesContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_commandes);
		try {
			setState(52);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(30);
				match(DESCR);
				setState(31);
				match(LPAR);
				setState(32);
				match(RPAR);
				monCircuit.descr();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(34);
				match(EVAL);
				setState(35);
				match(LPAR);
				setState(36);
				((CommandesContext)_localctx).liste_booleen = liste_booleen(0);
				setState(37);
				match(RPAR);
				monCircuit.eval(((CommandesContext)_localctx).liste_booleen.bools);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(40);
				match(DESCR);
				setState(41);
				match(LPAR);
				setState(42);
				match(RPAR);
				monCircuit.descr();
				setState(44);
				commandes();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(45);
				match(EVAL);
				setState(46);
				match(LPAR);
				setState(47);
				((CommandesContext)_localctx).liste_booleen = liste_booleen(0);
				setState(48);
				match(RPAR);
				monCircuit.eval(((CommandesContext)_localctx).liste_booleen.bools);
				setState(50);
				commandes();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Liste_idContext extends ParserRuleContext {
		public List<String> names;
		public Liste_idContext lsub;
		public Token ID;
		public TerminalNode ID() { return getToken(LeboolParser.ID, 0); }
		public TerminalNode VIRG() { return getToken(LeboolParser.VIRG, 0); }
		public Liste_idContext liste_id() {
			return getRuleContext(Liste_idContext.class,0);
		}
		public Liste_idContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_liste_id; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LeboolParserListener ) ((LeboolParserListener)listener).enterListe_id(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LeboolParserListener ) ((LeboolParserListener)listener).exitListe_id(this);
		}
	}

	public final Liste_idContext liste_id() throws RecognitionException {
		return liste_id(0);
	}

	private Liste_idContext liste_id(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Liste_idContext _localctx = new Liste_idContext(_ctx, _parentState);
		Liste_idContext _prevctx = _localctx;
		int _startState = 4;
		enterRecursionRule(_localctx, 4, RULE_liste_id, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(55);
			((Liste_idContext)_localctx).ID = match(ID);
			((Liste_idContext)_localctx).names =  new ArrayList<String>(); _localctx.names.add((((Liste_idContext)_localctx).ID!=null?((Liste_idContext)_localctx).ID.getText():null));
			}
			_ctx.stop = _input.LT(-1);
			setState(64);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Liste_idContext(_parentctx, _parentState);
					_localctx.lsub = _prevctx;
					_localctx.lsub = _prevctx;
					pushNewRecursionContext(_localctx, _startState, RULE_liste_id);
					setState(58);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(59);
					match(VIRG);
					setState(60);
					((Liste_idContext)_localctx).ID = match(ID);
					 ((Liste_idContext)_localctx).lsub.names.add((((Liste_idContext)_localctx).ID!=null?((Liste_idContext)_localctx).ID.getText():null)); ((Liste_idContext)_localctx).names = ((Liste_idContext)_localctx).lsub.names;
					}
					} 
				}
				setState(66);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class BooleenContext extends ParserRuleContext {
		public Boolean b;
		public TerminalNode TRUEBOOL() { return getToken(LeboolParser.TRUEBOOL, 0); }
		public TerminalNode FALSEBOOL() { return getToken(LeboolParser.FALSEBOOL, 0); }
		public BooleenContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_booleen; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LeboolParserListener ) ((LeboolParserListener)listener).enterBooleen(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LeboolParserListener ) ((LeboolParserListener)listener).exitBooleen(this);
		}
	}

	public final BooleenContext booleen() throws RecognitionException {
		BooleenContext _localctx = new BooleenContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_booleen);
		try {
			setState(71);
			switch (_input.LA(1)) {
			case TRUEBOOL:
				enterOuterAlt(_localctx, 1);
				{
				setState(67);
				match(TRUEBOOL);
				((BooleenContext)_localctx).b = Boolean.TRUE;
				}
				break;
			case FALSEBOOL:
				enterOuterAlt(_localctx, 2);
				{
				setState(69);
				match(FALSEBOOL);
				((BooleenContext)_localctx).b = Boolean.FALSE;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Liste_booleenContext extends ParserRuleContext {
		public List<Boolean> bools;
		public Liste_booleenContext lsub;
		public TerminalNode TRUEBOOL() { return getToken(LeboolParser.TRUEBOOL, 0); }
		public TerminalNode FALSEBOOL() { return getToken(LeboolParser.FALSEBOOL, 0); }
		public TerminalNode VIRG() { return getToken(LeboolParser.VIRG, 0); }
		public Liste_booleenContext liste_booleen() {
			return getRuleContext(Liste_booleenContext.class,0);
		}
		public Liste_booleenContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_liste_booleen; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LeboolParserListener ) ((LeboolParserListener)listener).enterListe_booleen(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LeboolParserListener ) ((LeboolParserListener)listener).exitListe_booleen(this);
		}
	}

	public final Liste_booleenContext liste_booleen() throws RecognitionException {
		return liste_booleen(0);
	}

	private Liste_booleenContext liste_booleen(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Liste_booleenContext _localctx = new Liste_booleenContext(_ctx, _parentState);
		Liste_booleenContext _prevctx = _localctx;
		int _startState = 8;
		enterRecursionRule(_localctx, 8, RULE_liste_booleen, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(78);
			switch (_input.LA(1)) {
			case TRUEBOOL:
				{
				setState(74);
				match(TRUEBOOL);
				((Liste_booleenContext)_localctx).bools =  new ArrayList<Boolean>(); _localctx.bools.add(Boolean.TRUE);
				}
				break;
			case FALSEBOOL:
				{
				setState(76);
				match(FALSEBOOL);
				((Liste_booleenContext)_localctx).bools =  new ArrayList<Boolean>(); _localctx.bools.add(Boolean.FALSE);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(90);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(88);
					switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
					case 1:
						{
						_localctx = new Liste_booleenContext(_parentctx, _parentState);
						_localctx.lsub = _prevctx;
						_localctx.lsub = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_liste_booleen);
						setState(80);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(81);
						match(VIRG);
						setState(82);
						match(TRUEBOOL);
						 ((Liste_booleenContext)_localctx).lsub.bools.add(Boolean.TRUE); ((Liste_booleenContext)_localctx).bools = ((Liste_booleenContext)_localctx).lsub.bools;
						}
						break;
					case 2:
						{
						_localctx = new Liste_booleenContext(_parentctx, _parentState);
						_localctx.lsub = _prevctx;
						_localctx.lsub = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_liste_booleen);
						setState(84);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(85);
						match(VIRG);
						setState(86);
						match(FALSEBOOL);
						 ((Liste_booleenContext)_localctx).lsub.bools.add(Boolean.FALSE); ((Liste_booleenContext)_localctx).bools = ((Liste_booleenContext)_localctx).lsub.bools;
						}
						break;
					}
					} 
				}
				setState(92);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public Composant compo;
		public ExprContext e1;
		public Token ID;
		public ExprContext e2;
		public TerminalNode NOT() { return getToken(LeboolParser.NOT, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode ID() { return getToken(LeboolParser.ID, 0); }
		public TerminalNode LPAR() { return getToken(LeboolParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(LeboolParser.RPAR, 0); }
		public TerminalNode AND() { return getToken(LeboolParser.AND, 0); }
		public TerminalNode OR() { return getToken(LeboolParser.OR, 0); }
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LeboolParserListener ) ((LeboolParserListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LeboolParserListener ) ((LeboolParserListener)listener).exitExpr(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 10;
		enterRecursionRule(_localctx, 10, RULE_expr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(105);
			switch (_input.LA(1)) {
			case NOT:
				{
				setState(94);
				match(NOT);
				setState(95);
				((ExprContext)_localctx).e1 = expr(2);
				Not n = new Not(); n.setIn(((ExprContext)_localctx).e1.compo); monCircuit.addComposant(n); ((ExprContext)_localctx).compo = n;
				}
				break;
			case ID:
				{
				setState(98);
				((ExprContext)_localctx).ID = match(ID);
				 ((ExprContext)_localctx).compo =  monCircuit.getInput((((ExprContext)_localctx).ID!=null?((ExprContext)_localctx).ID.getText():null));
				}
				break;
			case LPAR:
				{
				setState(100);
				match(LPAR);
				setState(101);
				((ExprContext)_localctx).e1 = expr(0);
				setState(102);
				match(RPAR);
				((ExprContext)_localctx).compo = ((ExprContext)_localctx).e1.compo;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(119);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(117);
					switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
					case 1:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						_localctx.e1 = _prevctx;
						_localctx.e1 = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(107);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(108);
						match(AND);
						setState(109);
						((ExprContext)_localctx).e2 = expr(5);
						And a = new And(); a.setIn1(((ExprContext)_localctx).e1.compo); a.setIn2(((ExprContext)_localctx).e2.compo); monCircuit.addComposant(a); ((ExprContext)_localctx).compo = a;
						}
						break;
					case 2:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						_localctx.e1 = _prevctx;
						_localctx.e1 = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(112);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(113);
						match(OR);
						setState(114);
						((ExprContext)_localctx).e2 = expr(4);
						Or o = new Or(); o.setIn1(((ExprContext)_localctx).e1.compo); o.setIn2(((ExprContext)_localctx).e2.compo); monCircuit.addComposant(o); ((ExprContext)_localctx).compo = o;
						}
						break;
					}
					} 
				}
				setState(121);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class EqContext extends ParserRuleContext {
		public Token ID;
		public ExprContext expr;
		public TerminalNode ID() { return getToken(LeboolParser.ID, 0); }
		public TerminalNode EQUAL() { return getToken(LeboolParser.EQUAL, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode PVIRG() { return getToken(LeboolParser.PVIRG, 0); }
		public EqContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_eq; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LeboolParserListener ) ((LeboolParserListener)listener).enterEq(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LeboolParserListener ) ((LeboolParserListener)listener).exitEq(this);
		}
	}

	public final EqContext eq() throws RecognitionException {
		EqContext _localctx = new EqContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_eq);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(122);
			((EqContext)_localctx).ID = match(ID);
			setState(123);
			match(EQUAL);
			setState(124);
			((EqContext)_localctx).expr = expr(0);
			setState(125);
			match(PVIRG);
			Vanne v = monCircuit.getOutput((((EqContext)_localctx).ID!=null?((EqContext)_localctx).ID.getText():null)); v.setIn(((EqContext)_localctx).expr.compo);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EquationsContext extends ParserRuleContext {
		public EqContext eq() {
			return getRuleContext(EqContext.class,0);
		}
		public EquationsContext equations() {
			return getRuleContext(EquationsContext.class,0);
		}
		public EquationsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equations; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LeboolParserListener ) ((LeboolParserListener)listener).enterEquations(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LeboolParserListener ) ((LeboolParserListener)listener).exitEquations(this);
		}
	}

	public final EquationsContext equations() throws RecognitionException {
		EquationsContext _localctx = new EquationsContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_equations);
		try {
			setState(132);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(128);
				eq();
				setState(129);
				equations();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(131);
				eq();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 2:
			return liste_id_sempred((Liste_idContext)_localctx, predIndex);
		case 4:
			return liste_booleen_sempred((Liste_booleenContext)_localctx, predIndex);
		case 5:
			return expr_sempred((ExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean liste_id_sempred(Liste_idContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean liste_booleen_sempred(Liste_booleenContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 2);
		case 2:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 3:
			return precpred(_ctx, 4);
		case 4:
			return precpred(_ctx, 3);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\22\u0089\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\3\2\3\2\3\2\3"+
		"\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3\67\n"+
		"\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\7\4A\n\4\f\4\16\4D\13\4\3\5\3\5\3\5"+
		"\3\5\5\5J\n\5\3\6\3\6\3\6\3\6\3\6\5\6Q\n\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6"+
		"\3\6\7\6[\n\6\f\6\16\6^\13\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\5\7l\n\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\7\7x\n\7\f\7\16"+
		"\7{\13\7\3\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\5\t\u0087\n\t\3\t\2\5"+
		"\6\n\f\n\2\4\6\b\n\f\16\20\2\2\u008d\2\22\3\2\2\2\4\66\3\2\2\2\68\3\2"+
		"\2\2\bI\3\2\2\2\nP\3\2\2\2\fk\3\2\2\2\16|\3\2\2\2\20\u0086\3\2\2\2\22"+
		"\23\7\21\2\2\23\24\7\t\2\2\24\25\5\6\4\2\25\26\b\2\1\2\26\27\7\n\2\2\27"+
		"\30\7\3\2\2\30\31\7\t\2\2\31\32\5\6\4\2\32\33\b\2\1\2\33\34\7\n\2\2\34"+
		"\35\5\20\t\2\35\36\7\4\2\2\36\37\5\4\3\2\37\3\3\2\2\2 !\7\5\2\2!\"\7\t"+
		"\2\2\"#\7\n\2\2#\67\b\3\1\2$%\7\b\2\2%&\7\t\2\2&\'\5\n\6\2\'(\7\n\2\2"+
		"()\b\3\1\2)\67\3\2\2\2*+\7\5\2\2+,\7\t\2\2,-\7\n\2\2-.\b\3\1\2.\67\5\4"+
		"\3\2/\60\7\b\2\2\60\61\7\t\2\2\61\62\5\n\6\2\62\63\7\n\2\2\63\64\b\3\1"+
		"\2\64\65\5\4\3\2\65\67\3\2\2\2\66 \3\2\2\2\66$\3\2\2\2\66*\3\2\2\2\66"+
		"/\3\2\2\2\67\5\3\2\2\289\b\4\1\29:\7\21\2\2:;\b\4\1\2;B\3\2\2\2<=\f\3"+
		"\2\2=>\7\17\2\2>?\7\21\2\2?A\b\4\1\2@<\3\2\2\2AD\3\2\2\2B@\3\2\2\2BC\3"+
		"\2\2\2C\7\3\2\2\2DB\3\2\2\2EF\7\6\2\2FJ\b\5\1\2GH\7\7\2\2HJ\b\5\1\2IE"+
		"\3\2\2\2IG\3\2\2\2J\t\3\2\2\2KL\b\6\1\2LM\7\6\2\2MQ\b\6\1\2NO\7\7\2\2"+
		"OQ\b\6\1\2PK\3\2\2\2PN\3\2\2\2Q\\\3\2\2\2RS\f\4\2\2ST\7\17\2\2TU\7\6\2"+
		"\2U[\b\6\1\2VW\f\3\2\2WX\7\17\2\2XY\7\7\2\2Y[\b\6\1\2ZR\3\2\2\2ZV\3\2"+
		"\2\2[^\3\2\2\2\\Z\3\2\2\2\\]\3\2\2\2]\13\3\2\2\2^\\\3\2\2\2_`\b\7\1\2"+
		"`a\7\r\2\2ab\5\f\7\4bc\b\7\1\2cl\3\2\2\2de\7\21\2\2el\b\7\1\2fg\7\t\2"+
		"\2gh\5\f\7\2hi\7\n\2\2ij\b\7\1\2jl\3\2\2\2k_\3\2\2\2kd\3\2\2\2kf\3\2\2"+
		"\2ly\3\2\2\2mn\f\6\2\2no\7\13\2\2op\5\f\7\7pq\b\7\1\2qx\3\2\2\2rs\f\5"+
		"\2\2st\7\f\2\2tu\5\f\7\6uv\b\7\1\2vx\3\2\2\2wm\3\2\2\2wr\3\2\2\2x{\3\2"+
		"\2\2yw\3\2\2\2yz\3\2\2\2z\r\3\2\2\2{y\3\2\2\2|}\7\21\2\2}~\7\16\2\2~\177"+
		"\5\f\7\2\177\u0080\7\20\2\2\u0080\u0081\b\b\1\2\u0081\17\3\2\2\2\u0082"+
		"\u0083\5\16\b\2\u0083\u0084\5\20\t\2\u0084\u0087\3\2\2\2\u0085\u0087\5"+
		"\16\b\2\u0086\u0082\3\2\2\2\u0086\u0085\3\2\2\2\u0087\21\3\2\2\2\f\66"+
		"BIPZ\\kwy\u0086";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}