

public abstract class Composant implements Comparable <Composant> {

	protected String id;
	
	public String getId() {
		
	 return super.toString(); 
	}

	public abstract String description();
	
	public abstract boolean getEtat() throws NonConnecteException;
	
	public String traceEtat(){
	
	try{
		return(description()+" etat: "+getEtat());
	}catch(NonConnecteException ex){
		return(getId()+" non connecte!");
	    }
	
	}
	
	  public int compareTo(Composant cps){
	  return (int) (this.getId().compareTo(cps.getId()));
	  }
}
