

public class Not extends Porte {
	
	protected Composant in;
	
	public void setIn(Composant comp) {
		
		in = comp;
		
	}

	public Composant getIn() {
		
		return in;
		
	}

	public String description(){

	    if(in == null){
	     return""+getId()+" in: non connecte";
	    }

	    return""+getId()+" in: "+in+"";
	}
	
	public boolean getEtat() throws NonConnecteException {
		
		if (in == null) {
			
			throw new NonConnecteException();
			
		} else {
			
			return !in.getEtat();
			
		}
	}
	
}
