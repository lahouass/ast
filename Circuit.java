import java.util.*;
public class Circuit{

	protected String nom;
	protected List <Composant> composants = new ArrayList<Composant>();
	protected SondesTable tableSondes = new SondesTable();

	
	 // Rajouter 
    public void ajout(Composant comp){
               composants.add(comp);
    }
    
    public Circuit(){
        super();
    }
	public Circuit(String nom, Composant[] cps){
	 this.nom = nom;
	 composants.addAll(Arrays.asList(cps));
	 Collections.sort(composants);
	}

	public List <String> nomeclature(){
		List <String> ids = new ArrayList<String>();
    	for(Composant c:composants){ 
	   		ids.add(c.getId()); 
			}
    	return ids;
	}

	public void traceEtats(){
	System.out.println("\nCircuit: "+nom+" etats:\n");
	for(Composant c : composants){
		System.out.println(c.traceEtat());
	 	} 
	}

	public List <Interrupteur> getIns(){
		List<Interrupteur> inter = new ArrayList<Interrupteur>();
		for(Composant c : composants) {
	    	if(c instanceof Interrupteur) {
			inter.add((Interrupteur)c);
	    	}
		}
		return inter;
	}

	public void description(){
		System.out.println("\nCircuit: "+nom+"\n");	
	 	for(Composant c:composants) {
	    	System.out.printf("%s \n",c.description());
			}
			
    }

	public List <Vanne> getOuts(){
		List<Vanne> van = new ArrayList<Vanne>();
		for(Composant c : composants) {
	    	if(c instanceof Vanne) {
			 van.add((Vanne)c);
	    	}
		}
		return van;
    }

    public void probe(SondesTable tableSondes){
    	this.tableSondes = tableSondes; 
    for(Composant c : composants) {
	    	if(c instanceof Not) {
			  if(((Not)c).getIn() instanceof Interrupteur){
			  LazySonde snd = tableSondes.getSonde((Interrupteur)((Not)c).getIn(),c,"in");
			  ((Not)c).setIn(snd);
			  }
	    	}
	    	if(c instanceof Or) {
			  if(((Porte2Entrees)c).getIn1() instanceof Interrupteur){
			  LazySonde snd = tableSondes.getSonde((Interrupteur)((Porte2Entrees)c).getIn1(),c,"in1");
			  ((Porte2Entrees)c).setIn1(snd);
			  }
			  if(((Porte2Entrees)c).getIn2() instanceof Interrupteur){
              LazySonde snd = tableSondes.getSonde((Interrupteur)((Porte2Entrees)c).getIn2(),c,"in2");
			  ((Porte2Entrees)c).setIn2(snd);
			  }
	    	}
            if(c instanceof And) {
			  if(((Porte2Entrees)c).getIn1() instanceof Interrupteur){
			  LazySonde snd = tableSondes.getSonde((Interrupteur)((Porte2Entrees)c).getIn1(),c,"in1");
			  ((Porte2Entrees)c).setIn1(snd);
			  }
			  if(((Porte2Entrees)c).getIn2() instanceof Interrupteur){
              LazySonde snd = tableSondes.getSonde((Interrupteur)((Porte2Entrees)c).getIn2(),c,"in2");
			  ((Porte2Entrees)c).setIn2(snd);
			  }
	    	}

		}
    }

    public void unProbe(SondesTable tableSondes){
    	this.tableSondes = tableSondes; 
    for(Composant c : composants) {
	    	if(c instanceof Not) {
			  if(((Not)c).getIn() instanceof LazySonde){
			  Interrupteur inter = tableSondes.getInterrupteur((LazySonde)((Not)c).getIn());
			  if(inter != null){((Not)c).setIn(inter);}
			  }
	    	}
	    	if(c instanceof Or) {
			  if(((Porte2Entrees)c).getIn1() instanceof LazySonde){
			  Interrupteur inter = tableSondes.getInterrupteur((LazySonde)((Porte2Entrees)c).getIn1());
			  if(inter != null){((Porte2Entrees)c).setIn1(inter);}
			  }
			  if(((Porte2Entrees)c).getIn2() instanceof LazySonde){
             Interrupteur inter = tableSondes.getInterrupteur((LazySonde)((Porte2Entrees)c).getIn2());
			  if(inter != null){((Porte2Entrees)c).setIn2(inter);}
			  }
	    	}
            if(c instanceof And) {
			  if(((Porte2Entrees)c).getIn1() instanceof LazySonde){
			  Interrupteur inter = tableSondes.getInterrupteur((LazySonde)((Porte2Entrees)c).getIn1());
			  if(inter != null){((Porte2Entrees)c).setIn1(inter);}
			  }
			  if(((Porte2Entrees)c).getIn2() instanceof LazySonde){
             Interrupteur inter = tableSondes.getInterrupteur((LazySonde)((Porte2Entrees)c).getIn2());
			  if(inter != null){((Porte2Entrees)c).setIn2(inter);}
			  }
	    	}

		}
    }
	
} 
