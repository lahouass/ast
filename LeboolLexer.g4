
lexer grammar LeboolLexer;

RETURNS : 'returns';
END : 'end';
DESCR : 'descr';
TRUEBOOL : 'true';
FALSEBOOL : 'false';
EVAL : 'eval';

LPAR : '(';

RPAR : ')';

AND : '&';

OR : '|';

NOT : '!';

EQUAL : '=';

VIRG : ',';

PVIRG : ';' ;

ID : [a-zA-Z][a-zA-Z0-9]*;

WS : [ \t\r\n]+ -> skip ;


        
