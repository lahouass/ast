
public class Interrupteur extends Composant {

  public String description(){
  return(getId());
  }
  
	protected boolean etat;
	String name;

	public Interrupteur(){
	    super();
	}
	
	public Interrupteur(String name){
        super();
        this.name = name;
	}
	
	public void on() {
		
		etat = true;
		
	}    
	
	public void off() {
		
		etat = false;
		
	}
	
	public boolean getEtat() throws NonConnecteException {
		
		return etat;
		
	}
	
	
	
}
