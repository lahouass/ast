// Generated from LeboolParser.g4 by ANTLR 4.5
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link LeboolParser}.
 */
public interface LeboolParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link LeboolParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProg(LeboolParser.ProgContext ctx);
	/**
	 * Exit a parse tree produced by {@link LeboolParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProg(LeboolParser.ProgContext ctx);
	/**
	 * Enter a parse tree produced by {@link LeboolParser#commandes}.
	 * @param ctx the parse tree
	 */
	void enterCommandes(LeboolParser.CommandesContext ctx);
	/**
	 * Exit a parse tree produced by {@link LeboolParser#commandes}.
	 * @param ctx the parse tree
	 */
	void exitCommandes(LeboolParser.CommandesContext ctx);
	/**
	 * Enter a parse tree produced by {@link LeboolParser#liste_id}.
	 * @param ctx the parse tree
	 */
	void enterListe_id(LeboolParser.Liste_idContext ctx);
	/**
	 * Exit a parse tree produced by {@link LeboolParser#liste_id}.
	 * @param ctx the parse tree
	 */
	void exitListe_id(LeboolParser.Liste_idContext ctx);
	/**
	 * Enter a parse tree produced by {@link LeboolParser#booleen}.
	 * @param ctx the parse tree
	 */
	void enterBooleen(LeboolParser.BooleenContext ctx);
	/**
	 * Exit a parse tree produced by {@link LeboolParser#booleen}.
	 * @param ctx the parse tree
	 */
	void exitBooleen(LeboolParser.BooleenContext ctx);
	/**
	 * Enter a parse tree produced by {@link LeboolParser#liste_booleen}.
	 * @param ctx the parse tree
	 */
	void enterListe_booleen(LeboolParser.Liste_booleenContext ctx);
	/**
	 * Exit a parse tree produced by {@link LeboolParser#liste_booleen}.
	 * @param ctx the parse tree
	 */
	void exitListe_booleen(LeboolParser.Liste_booleenContext ctx);
	/**
	 * Enter a parse tree produced by {@link LeboolParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(LeboolParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link LeboolParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(LeboolParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link LeboolParser#eq}.
	 * @param ctx the parse tree
	 */
	void enterEq(LeboolParser.EqContext ctx);
	/**
	 * Exit a parse tree produced by {@link LeboolParser#eq}.
	 * @param ctx the parse tree
	 */
	void exitEq(LeboolParser.EqContext ctx);
	/**
	 * Enter a parse tree produced by {@link LeboolParser#equations}.
	 * @param ctx the parse tree
	 */
	void enterEquations(LeboolParser.EquationsContext ctx);
	/**
	 * Exit a parse tree produced by {@link LeboolParser#equations}.
	 * @param ctx the parse tree
	 */
	void exitEquations(LeboolParser.EquationsContext ctx);
}