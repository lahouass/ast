

public class Vanne extends Composant {
	
	protected Composant in;
	String name;
	
	public Vanne(){
	    super();
	}
	
	public Vanne(String name){
        super();
        this.name = name;
	}
	
	public void setIn(Composant comp) {
		
		in = comp;
		
	}
	
	

	public String description(){

	    if(in == null){
	     return""+getId()+" in: non connecte";
	    }

	    return""+getId()+" in: "+in+"";
	}
	
	public boolean getEtat() throws NonConnecteException {
		
		if (in == null) {
			
			throw new NonConnecteException();
			
		} else {
			
			return in.getEtat();
			
		}
	} 
	
}
