

public abstract class Porte2Entrees extends Porte {

	protected Composant in1;
	protected Composant in2;
	
	
    public abstract boolean eval() throws NonConnecteException;

    public void setIn1(Composant comp) {
		
		in1 = comp;
		
	}
	
	public void setIn2(Composant comp) {
		
		in2 = comp;
		
	}

	public Composant getIn1() {
		
		return in1;
		
	}
	
	public Composant getIn2() {
		
		return in2;
		
	}

	    public String description(){

	    if(in1 != null && in2 != null){
	     return""+getId()+" in1: "+in1+" in2: "+in2+"";
	    }

	    else if(in1 != null && in2 == null){
	     return""+getId()+" in1: "+in1+" in2: non connecte";
	    }
	    else if(in1 == null && in2 != null){
	     return""+getId()+" in1: non connecte in2: "+in2+"";
	    }
	    return""+getId()+" in1: non connecte in2: non connecte";
	}
	
	public boolean getEtat() throws NonConnecteException {
		if (in1 == null || in2 == null) {
			
			throw new NonConnecteException();
			
		} else {
			return eval();
			
		}
	}
	
	
}
