
# AST (Abstract Syntax Tree) 

**main files**
* LeboolLexer.g4
* LeboolParser.g4
* test
* EquationCircuit.java
* All others .java files (And, Or, Not...)

**How to use it ?**
In an terminal
1. antlr4 LeboolLexer.g4 LeboolParser.g4
2. javac *.java
3. grun Lebool prog test


