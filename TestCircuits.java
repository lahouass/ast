

import java.util.*;

public class TestCircuits{

	static SondesTable tableSondes = new SondesTable();

	public static void printIds(Composant[] tab){
	 for(Composant c:tab) //c parcourt le composant tab
	 {
	    System.out.printf("Id: %s \n",c.getId());
	 }
	}

	public static void descriptions(Composant[] tab){
	 for(Composant c:tab) //c parcourt le composant tab
	 {
	    System.out.printf("%s \n",c.description());
	 }
	}
	
	public static void traceEtats(Composant[] tab){
	 for(Composant c:tab) //c parcourt le composant tab
	 {   
	      System.out.printf("%s \n",c.traceEtat());
	 }
	}

	static void test(Circuit circ) {

		List<String> nomenclature = new ArrayList<String>();
		nomenclature = circ.nomeclature();
		System.out.println("Nomenclature du circuit \n");
		for(String nom : nomenclature) {
		    System.out.println(nom);
		}

		circ.description();

		List<Interrupteur> interu = new ArrayList<Interrupteur>();
		interu = circ.getIns();
		System.out.println("\nInterrupteurs du circuit \n");
		for(Interrupteur nom : interu) {
		    System.out.println(nom.getId());
		}
		List<Vanne> van = new ArrayList<Vanne>();
		van = circ.getOuts();
		System.out.println("\nVannes du circuit \n");
		for(Vanne nom : van) {
		    System.out.println(nom.getId());
		}

        circ.traceEtats();
		circ.probe(tableSondes);
		circ.traceEtats();
		//tableSondes.clear();
		circ.unProbe(tableSondes);
		circ.traceEtats();		
      }
	
	public static void main(String[] args){
	//Construction
	Composant[] tabComp = new Composant[7];
	tabComp[0] = new Interrupteur();
	tabComp[1] = new Interrupteur();	
	tabComp[2] = new Interrupteur();
	tabComp[3] = new Or();
	tabComp[4] = new Not();
	tabComp[5] = new And();
	tabComp[6] = new Vanne();
	
	//Connexions
	((Porte2Entrees)tabComp[3]).setIn1(tabComp[0]);
	((Porte2Entrees)tabComp[3]).setIn2(tabComp[1]);
	((Not)tabComp[4]).setIn(tabComp[2]);	
	((Porte2Entrees)tabComp[5]).setIn1(tabComp[3]);
	((Porte2Entrees)tabComp[5]).setIn2(tabComp[4]);
	((Vanne)tabComp[6]).setIn(tabComp[5]);

	((Interrupteur)tabComp[0]).on();
	((Interrupteur)tabComp[1]).off();	
	((Interrupteur)tabComp[2]).on();


	//Nouveau circuit
	Circuit circuit = new Circuit("Circuit vanne1",tabComp);
	
	//Affichage
	 //traceEtats(tabComp);
	 test(circuit);
	System.out.println("Au revoir!");
	}
	
}
