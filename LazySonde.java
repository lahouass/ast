
import java.util.InputMismatchException;
public class LazySonde extends Sonde {

        
	boolean b, i = false;
	public LazySonde(Composant comp, String str){
	 super(comp,str);
	}
	
	public boolean getEtat() throws InputMismatchException{
	  if(i == false){
	  b = super.getEtat();
	  i = true;
	  return b;
	  }else{
	  return b;}
	}
	
	public void reset(){
	 i = false;
	}

}
