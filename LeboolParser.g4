
parser grammar LeboolParser;

options {tokenVocab=LeboolLexer;}

@members { EquationCircuit monCircuit = new EquationCircuit();}

// la règle

prog : ID LPAR liste_id {monCircuit.setInputs($liste_id.names);} RPAR RETURNS LPAR liste_id {monCircuit.setOutputs($liste_id.names);} RPAR equations 
       END
       commandes;

       
commandes : DESCR LPAR RPAR {monCircuit.descr();} 
            | EVAL LPAR liste_booleen RPAR {monCircuit.eval($liste_booleen.bools);} 
            | DESCR LPAR RPAR {monCircuit.descr();} commandes
            | EVAL LPAR liste_booleen RPAR {monCircuit.eval($liste_booleen.bools);} commandes;


liste_id returns [List<String> names]: 
        ID {$names = new ArrayList<String>(); $names.add($ID.text);}
        |lsub=liste_id VIRG ID { $lsub.names.add($ID.text); $names=$lsub.names;};

booleen returns [Boolean b]:
        TRUEBOOL{$b=Boolean.TRUE;}
        |FALSEBOOL{$b=Boolean.FALSE;};
 
liste_booleen returns [List<Boolean> bools]: 
        TRUEBOOL {$bools = new ArrayList<Boolean>(); $bools.add(Boolean.TRUE);} 
        | FALSEBOOL {$bools = new ArrayList<Boolean>(); $bools.add(Boolean.FALSE);}
        |lsub=liste_booleen VIRG TRUEBOOL { $lsub.bools.add(Boolean.TRUE); $bools=$lsub.bools;}
        |lsub=liste_booleen VIRG FALSEBOOL { $lsub.bools.add(Boolean.FALSE); $bools=$lsub.bools;};

expr returns [Composant compo] : ID { $compo = monCircuit.getInput($ID.text);} 
|e1=expr AND e2=expr {And a = new And(); a.setIn1($e1.compo); a.setIn2($e2.compo); monCircuit.addComposant(a); $compo=a;}  
|e1=expr OR e2=expr  {Or o = new Or(); o.setIn1($e1.compo); o.setIn2($e2.compo); monCircuit.addComposant(o); $compo=o;}  
|NOT e1=expr {Not n = new Not(); n.setIn($e1.compo); monCircuit.addComposant(n); $compo=n;}
|LPAR e1=expr RPAR {$compo=$e1.compo;}
; 

eq : ID EQUAL expr PVIRG {Vanne v = monCircuit.getOutput($ID.text); v.setIn($expr.compo);};

equations : eq equations
            |eq;